/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import logica.Productos;

/**
 *
 * @author iamramiroo
 */
@Named(value = "controladorProducto")
@SessionScoped
public class ControladorProducto implements Serializable {

    private List<Productos> listaProductos;
    Productos product = new Productos();
    String search = "";


    /**
     * Creates a new instance of ControladorProducto
     */
    public ControladorProducto() {
        cargarProductos();
    }

    private void cargarProductos() {
        listaProductos = null;
        
        EntityManager em = product.getEntityManager();
        TypedQuery<Productos> consultaProductos = em.createNamedQuery("Productos.findAll", Productos.class);
        
        listaProductos = consultaProductos.getResultList();
        
    }
    
    
    public void crearProducto() {
        EntityManager em = product.getEntityManager();
        em.getTransaction().begin();
        em.persist(product);
        em.getTransaction().commit();

        product = new Productos();
        cargarProductos();
    }
    
    public void buscarProductos(){
        System.err.println("Buscando "+ search);
    }
    
    public void editarProducto(int id){
        
    }

    public Productos getProduct() {
        return product;
    }

    public void setProduct(Productos product) {
        this.product = product;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
    
    public List<Productos> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Productos> listaProductos) {
        this.listaProductos = listaProductos;
    }
    
    
}
