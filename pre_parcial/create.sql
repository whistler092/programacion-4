
DROP TABLE `matriculas`.`Enrollments`;
DROP TABLE `matriculas`.`AcademicPrograms`;
DROP TABLE `matriculas`.`Students`;

CREATE TABLE `matriculas`.`Students` ( 
  `idStudent` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `code` VARCHAR(45) NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`idStudent`));

CREATE TABLE `matriculas`.`AcademicPrograms` (
  `idAcademicProgram` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `faculty` VARCHAR(45) NULL,
  PRIMARY KEY (`idAcademicProgram`));


CREATE TABLE `matriculas`.`Enrollments` (
  `idEnrollment` INT NOT NULL AUTO_INCREMENT,
  `idAcademicProgram` INT NULL,
  `idStudent` INT NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`idEnrollment`),
  INDEX `fk_Enrollments_1_idx` (`idAcademicProgram` ASC),
  CONSTRAINT `fk_Enrollments_AcademicPrograms`
    FOREIGN KEY (`idAcademicProgram`)
    REFERENCES `matriculas`.`AcademicPrograms` (`idAcademicProgram`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `matriculas`.`Enrollments` 
ADD INDEX `fk_Enrollments_Student_idx` (`idStudent` ASC);
ALTER TABLE `matriculas`.`Enrollments` 
ADD CONSTRAINT `fk_Enrollments_Student`
  FOREIGN KEY (`idStudent`)
  REFERENCES `matriculas`.`Students` (`idStudent`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


INSERT INTO `matriculas`.`Students` (`idStudent`,`name`,`code`,`date`)
VALUES (1,'Ramiro Andrés Bedoya', '1143838715' , CURRENT_DATE());

INSERT INTO `matriculas`.`AcademicPrograms` (`idAcademicProgram`, `name`,`faculty`)
VALUES (1 , 'Ingeniería de Software', 'Ingenieria');

INSERT INTO `matriculas`.`Enrollments` (`idEnrollment`, `idAcademicProgram`, `idStudent`,`date`)
VALUES (1 , 1 , 1 , current_date());



