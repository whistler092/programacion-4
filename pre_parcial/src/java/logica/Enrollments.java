/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author java-ws
 */
@Entity
@Table(name = "Enrollments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enrollments.findAll", query = "SELECT e FROM Enrollments e")
    , @NamedQuery(name = "Enrollments.findByIdEnrollment", query = "SELECT e FROM Enrollments e WHERE e.idEnrollment = :idEnrollment")
    , @NamedQuery(name = "Enrollments.findByDate", query = "SELECT e FROM Enrollments e WHERE e.date = :date")})
public class Enrollments implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEnrollment")
    private Integer idEnrollment;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "idAcademicProgram", referencedColumnName = "idAcademicProgram")
    @ManyToOne
    private AcademicPrograms idAcademicProgram;
    @JoinColumn(name = "idStudent", referencedColumnName = "idStudent")
    @ManyToOne
    private Students idStudent;

    public Enrollments() {
    }

    public Enrollments(Integer idEnrollment) {
        this.idEnrollment = idEnrollment;
    }

    public Integer getIdEnrollment() {
        return idEnrollment;
    }

    public void setIdEnrollment(Integer idEnrollment) {
        this.idEnrollment = idEnrollment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AcademicPrograms getIdAcademicProgram() {
        return idAcademicProgram;
    }

    public void setIdAcademicProgram(AcademicPrograms idAcademicProgram) {
        this.idAcademicProgram = idAcademicProgram;
    }

    public Students getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Students idStudent) {
        this.idStudent = idStudent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnrollment != null ? idEnrollment.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enrollments)) {
            return false;
        }
        Enrollments other = (Enrollments) object;
        if ((this.idEnrollment == null && other.idEnrollment != null) || (this.idEnrollment != null && !this.idEnrollment.equals(other.idEnrollment))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "logica.Enrollments[ idEnrollment=" + idEnrollment + " ]";
    }
    
}
