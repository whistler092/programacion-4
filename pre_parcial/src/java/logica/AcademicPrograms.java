/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author java-ws
 */
@Entity
@Table(name = "AcademicPrograms")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcademicPrograms.findAll", query = "SELECT a FROM AcademicPrograms a")
    , @NamedQuery(name = "AcademicPrograms.findByIdAcademicProgram", query = "SELECT a FROM AcademicPrograms a WHERE a.idAcademicProgram = :idAcademicProgram")
    , @NamedQuery(name = "AcademicPrograms.findByName", query = "SELECT a FROM AcademicPrograms a WHERE a.name = :name")
    , @NamedQuery(name = "AcademicPrograms.findByFaculty", query = "SELECT a FROM AcademicPrograms a WHERE a.faculty = :faculty")})
public class AcademicPrograms implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAcademicProgram")
    private Integer idAcademicProgram;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "faculty")
    private String faculty;
    @OneToMany(mappedBy = "idAcademicProgram")
    private Collection<Enrollments> enrollmentsCollection;

    public AcademicPrograms() {
    }

    public AcademicPrograms(Integer idAcademicProgram) {
        this.idAcademicProgram = idAcademicProgram;
    }

    public Integer getIdAcademicProgram() {
        return idAcademicProgram;
    }

    public void setIdAcademicProgram(Integer idAcademicProgram) {
        this.idAcademicProgram = idAcademicProgram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @XmlTransient
    public Collection<Enrollments> getEnrollmentsCollection() {
        return enrollmentsCollection;
    }

    public void setEnrollmentsCollection(Collection<Enrollments> enrollmentsCollection) {
        this.enrollmentsCollection = enrollmentsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAcademicProgram != null ? idAcademicProgram.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcademicPrograms)) {
            return false;
        }
        AcademicPrograms other = (AcademicPrograms) object;
        if ((this.idAcademicProgram == null && other.idAcademicProgram != null) || (this.idAcademicProgram != null && !this.idAcademicProgram.equals(other.idAcademicProgram))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "logica.AcademicPrograms[ idAcademicProgram=" + idAcademicProgram + " ]";
    }
    
}
