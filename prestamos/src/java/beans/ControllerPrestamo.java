/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import logica.Prestamo;

/**
 *
 * @author java-ws
 */
@Named(value = "controllerPrestamo")
@SessionScoped
public class ControllerPrestamo implements Serializable {

    /**
     * Creates a new instance of ControllerPrestamo
     */
    List<Prestamo> prestamos;
    Prestamo selectedPrestamo;
    
    public ControllerPrestamo() {
        this.selectedPrestamo = new Prestamo();
        cargarTodos();
    }
    
    public void crearPrestamo(){
        EntityManager em = selectedPrestamo.getEntityManager();
        em.getTransaction().begin();
        selectedPrestamo.setEstado("Activo");
        em.persist(selectedPrestamo);
        em.getTransaction().commit();
        
        cargarTodos();
        
    }
    
    public void cargarTodos(){
        getListaPrestamos();
    }
    
    private List<Prestamo> getListaPrestamos() {
        EntityManager em = selectedPrestamo.getEntityManager();
        TypedQuery<Prestamo> consultaPrestamos = em.createNamedQuery("Prestamo.findAll", Prestamo.class);
        
        prestamos = consultaPrestamos.getResultList();
        return prestamos;
    }
    
    public List<Prestamo> getPrestamos() {
        return prestamos;
    }

    public void setPrestamos(List<Prestamo> prestamos) {
        this.prestamos = prestamos;
    }

    public Prestamo getSelectedPrestamo() {
        return selectedPrestamo;
    }

    public void setSelectedPrestamo(Prestamo selectedPrestamo) {
        this.selectedPrestamo = selectedPrestamo;
    }
    
}
