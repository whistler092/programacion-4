/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import logica.Prestamo;
import logica.Usuario;

/**
 *
 * @author java-ws
 */
@Named(value = "controllerUsuario")
@SessionScoped
public class ControllerUsuario implements Serializable {

    /**
     * Creates a new instance of ControllerUsuario
     */
    List<Usuario> listaUsuarios;
    Usuario selectedUsuarios;

    public ControllerUsuario() {

        this.selectedUsuarios = new Usuario();
        getListaPrestamos();
    }

    public void crearUsuario() {
    }

    private List<Usuario> getListaPrestamos() {
        EntityManager em = selectedUsuarios.getEntityManager();
        TypedQuery<Usuario> consultaUsuarios = em.createNamedQuery("Usuario.findAll", Usuario.class);

        listaUsuarios = consultaUsuarios.getResultList();
        return listaUsuarios;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public Usuario getSelectedUsuarios() {
        return selectedUsuarios;
    }

    public void setSelectedUsuarios(Usuario selectedUsuarios) {
        this.selectedUsuarios = selectedUsuarios;
    }

}
