/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import logica.Item;

/**
 *
 * @author java-ws
 */
@Named(value = "controllerItem")
@SessionScoped
public class ControllerItem implements Serializable {

    /**
     * Creates a new instance of ControllerItem
     */
    List<Item> items;
    Item selectedItem;

    public ControllerItem() {
        selectedItem = new Item();
        cargarTodos();
    }

    public void crearItem() {
        EntityManager em = selectedItem.getEntityManager();
        em.getTransaction().begin();
        selectedItem.setEstado(true);
        em.persist(selectedItem);
        em.getTransaction().commit();

        cargarTodos();
        selectedItem = new Item();
    }

    public void cargarTodos() {

        EntityManager em = selectedItem.getEntityManager();
        TypedQuery<Item> consultaItems = em.createNamedQuery("Item.findAll", Item.class);

        items = consultaItems.getResultList();
    }

    public List<Item> getListaItem() {
        EntityManager em = selectedItem.getEntityManager();
        TypedQuery<Item> consultaItems = em.createNamedQuery("Item.findAll", Item.class);

        return consultaItems.getResultList();
    }

    public Item getItemById(int id) {
        EntityManager em = selectedItem.getEntityManager();
        TypedQuery<Item> consultaItems = em.createNamedQuery("Item.findById", Item.class);
        consultaItems.setParameter("id", id);

        return consultaItems.getResultList().get(0);
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Item selectedItem) {
        this.selectedItem = selectedItem;
    }

}
