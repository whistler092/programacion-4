/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import logica.Usuario;

/**
 *
 * @author ramir
 */
@Named(value = "beanUsuario")
@SessionScoped
public class BeanUsuario implements Serializable {

    private Usuario usuario = new Usuario();
    private String selectedId = "";
    private ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();

    /**
     * Creates a new instance of BeanUsuario
     */
    public BeanUsuario() {
    }

    //Logic 
    public void crearUsuario() {
        listaUsuarios.add(usuario);
        usuario = new Usuario();
    }

    public String editarUsuario(String cedula) {
        usuario = new Usuario();
        for (Usuario usr : listaUsuarios) {
            if (usr.getCedula() == cedula) {
                usuario = usr;
            }
        }

        return "editar";
    }

    public String actualizarUsuario() {

        for (Usuario usr : listaUsuarios) {
            if (usr.getCedula() == null ? usuario.getCedula() == null : usr.getCedula().equals(usuario.getCedula())) {
                usr = usuario;
            }
        }
        return "listado";

    }

    public String deleteUsuario(String cedula) {

        /*listaUsuarios.stream().filter((usr) -> (usr.getCedula() == null ? cedula == null : usr.getCedula().equals(cedula))).forEachOrdered((usr) -> {
            usuario = usr;
        });*/

        for (int i = 0; i < listaUsuarios.size(); i++) {

            if (listaUsuarios.get(i).getCedula().equals(cedula)) {
                listaUsuarios.remove(i);
                i--;
            }
        }

        return "listado";
    }

    public String verListado() {
        return "listado";
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the listaUsuarios
     */
    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    /**
     * @param listaUsuarios the listaUsuarios to set
     */
    public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    /**
     * @return the selectedId
     */
    public String getSelectedId() {
        return selectedId;
    }

    /**
     * @param selectedId the selectedId to set
     */
    public void setSelectedId(String selectedId) {
        this.selectedId = selectedId;
    }

    public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        if (((String) arg2).length() < 5) {
            System.out.println(arg1.getId());
            throw new ValidatorException(new FacesMessage("Al menos 5 caracteres "));
        }
    }
}
