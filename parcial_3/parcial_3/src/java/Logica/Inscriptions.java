/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author java-ws
 */
@Entity
@Table(name = "Inscriptions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inscriptions.findAll", query = "SELECT i FROM Inscriptions i")
    , @NamedQuery(name = "Inscriptions.findByIdInscriptions", query = "SELECT i FROM Inscriptions i WHERE i.idInscriptions = :idInscriptions")
    , @NamedQuery(name = "Inscriptions.findByCDate", query = "SELECT i FROM Inscriptions i WHERE i.cDate = :cDate")
    , @NamedQuery(name = "Inscriptions.findByProgram", query = "SELECT i FROM Inscriptions i WHERE i.program = :program")
    , @NamedQuery(name = "Inscriptions.findByPINCode", query = "SELECT i FROM Inscriptions i WHERE i.pINCode = :pINCode")
    , @NamedQuery(name = "Inscriptions.findAdmited" , query = "SELECT i FROM Inscriptions i WHERE i.program = :Program ORDER BY i.iCFESScore DESC")
    , @NamedQuery(name = "Inscriptions.findByICFESScore", query = "SELECT i FROM Inscriptions i WHERE i.iCFESScore = :iCFESScore")})
public class Inscriptions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idInscriptions")
    private Integer idInscriptions;
    @Column(name = "cDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cDate;
    @Size(max = 45)
    @Column(name = "program")
    private String program;
    @Size(max = 45)
    @Column(name = "PINCode")
    private String pINCode;
    @Size(max = 45)
    @Column(name = "ICFESScore")
    private String iCFESScore;
    @JoinColumn(name = "idStudent", referencedColumnName = "idStudent")
    @ManyToOne
    private Students idStudent;

    public Inscriptions() {
    }

    public Inscriptions(Integer idInscriptions) {
        this.idInscriptions = idInscriptions;
    }

    public Integer getIdInscriptions() {
        return idInscriptions;
    }

    public void setIdInscriptions(Integer idInscriptions) {
        this.idInscriptions = idInscriptions;
    }

    public Date getCDate() {
        return cDate;
    }

    public void setCDate(Date cDate) {
        this.cDate = cDate;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getPINCode() {
        return pINCode;
    }

    public void setPINCode(String pINCode) {
        this.pINCode = pINCode;
    }

    public String getICFESScore() {
        return iCFESScore;
    }

    public void setICFESScore(String iCFESScore) {
        this.iCFESScore = iCFESScore;
    }

    public Students getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Students idStudent) {
        this.idStudent = idStudent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInscriptions != null ? idInscriptions.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inscriptions)) {
            return false;
        }
        Inscriptions other = (Inscriptions) object;
        if ((this.idInscriptions == null && other.idInscriptions != null) || (this.idInscriptions != null && !this.idInscriptions.equals(other.idInscriptions))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Logica.Inscriptions[ idInscriptions=" + idInscriptions + " ]";
    }
    
    public EntityManager getEntityManager(){
        //parcial_3PU
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("parcial_3PU");
        EntityManager em = emf.createEntityManager();
        return em;
    }
    
}
