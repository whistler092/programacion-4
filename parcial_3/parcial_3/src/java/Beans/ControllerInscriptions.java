/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Logica.Inscriptions;
import Logica.Students;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author java-ws
 */
@Named(value = "controllerInscriptions")
@SessionScoped
public class ControllerInscriptions implements Serializable {

    private String filtrarProgramaSeleccionado;
    private Inscriptions selectedInscription;
    private List<Inscriptions> listInscriptions;
    private List<Inscriptions> listInscriptionsAdmitted;
    
    private Students selectedStudent;

    private List<Students> listStudents;

    /**
     * Creates a new instance of ControllerInscriptions
     */
    public ControllerInscriptions() {
        selectedInscription = new Inscriptions();
        loadInscriptions();
        loadStudents();
        loadStudentByProgram();
    }

    public void createInscription() {

        EntityManager em = selectedInscription.getEntityManager();
        em.getTransaction().begin();
        em.persist(selectedInscription);
        em.getTransaction().commit();

        selectedInscription = new Inscriptions();
        loadInscriptions();

    }
    
    public void filterByProgram(){
        
    }

    public void loadStudents() {
        listStudents = null;

        EntityManager em = selectedInscription.getEntityManager();
        TypedQuery<Students> consultaStudents = em.createNamedQuery("Students.findAll", Students.class);
        listStudents = consultaStudents.getResultList();
        
    }
    
    public void loadStudentByProgram(){
        if(filtrarProgramaSeleccionado == null)
        {
            filtrarProgramaSeleccionado = "Ing. Sistemas";
        }
        listInscriptionsAdmitted = null;
        EntityManager em = selectedInscription.getEntityManager();
        TypedQuery<Inscriptions> consultaInscritosAdmitidos = em.createNamedQuery("Inscriptions.findAdmited" , Inscriptions.class);
        consultaInscritosAdmitidos.setParameter("Program", filtrarProgramaSeleccionado);
        
        listInscriptionsAdmitted = consultaInscritosAdmitidos.getResultList();
        
        if(listInscriptionsAdmitted.size() > 5)
        {
            listInscriptionsAdmitted = listInscriptionsAdmitted.subList(0, 5);
        }
    }
    public void loadStudentByProgramAll(){
        
        if(filtrarProgramaSeleccionado == null)
        {
            filtrarProgramaSeleccionado = "Ing. Sistemas";
        }
        listInscriptionsAdmitted = null;
        EntityManager em = selectedInscription.getEntityManager();
        TypedQuery<Inscriptions> consultaInscritosAdmitidos = em.createNamedQuery("Inscriptions.findAdmited" , Inscriptions.class);
        consultaInscritosAdmitidos.setParameter("Program", filtrarProgramaSeleccionado);
        
        listInscriptionsAdmitted = consultaInscritosAdmitidos.getResultList();
        
    }
    

    public void loadInscriptions() {
        listInscriptions = null;

        EntityManager em = selectedInscription.getEntityManager();
        TypedQuery<Inscriptions> consultaInscripciones = em.createNamedQuery("Inscriptions.findAll", Inscriptions.class);
        listInscriptions = consultaInscripciones.getResultList();
    }

    public Inscriptions getSelectedInscription() {
        return selectedInscription;
    }

    public void setSelectedInscription(Inscriptions selectedInscription) {
        this.selectedInscription = selectedInscription;
    }

    public List<Inscriptions> getListInscriptions() {
        return listInscriptions;
    }

    public void setListInscriptions(List<Inscriptions> listInscriptions) {
        this.listInscriptions = listInscriptions;
    }

    public List<Students> getListStudents() {
        return listStudents;
    }

    public void setListStudents(List<Students> listStudents) {
        this.listStudents = listStudents;
    }

    public Students getSelectedStudent() {
        return selectedStudent;
    }

    public void setSelectedStudent(Students selectedStudent) {
        this.selectedStudent = selectedStudent;
    }

    public List<Inscriptions> getListInscriptionsAdmitted() {
        return listInscriptionsAdmitted;
    }

    public void setListInscriptionsAdmitted(List<Inscriptions> listInscriptionsAdmitted) {
        this.listInscriptionsAdmitted = listInscriptionsAdmitted;
    }

    public String getFiltrarProgramaSeleccionado() {
        return filtrarProgramaSeleccionado;
    }

    public void setFiltrarProgramaSeleccionado(String filtrarProgramaSeleccionado) {
        this.filtrarProgramaSeleccionado = filtrarProgramaSeleccionado;
    }
    
    

}
