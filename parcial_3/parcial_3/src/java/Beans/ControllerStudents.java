/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Logica.Students;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author java-ws
 */
@Named(value = "controllerStudents")
@SessionScoped
public class ControllerStudents implements Serializable {

    private Students selectedStudent;
    private List<Students> listStudents;
    private List<Students> listStudentsByProgram;

    /**
     * Creates a new instance of ControllerStudents
     */
    public ControllerStudents() {
        selectedStudent = new Students();
        loadStudents();
    }

    public void editStudent(Students student) {
        selectedStudent = student;
    }

    public String getStatusName() {
        return selectedStudent.getIdStudent() == null ? "Crear" : "Editar";
    }

    public void createStudent() {

        if (selectedStudent.getIdStudent() == null) {

            EntityManager em = selectedStudent.getEntityManager();
            em.getTransaction().begin();
            em.persist(selectedStudent);
            em.getTransaction().commit();
        } else {

            EntityManager em = selectedStudent.getEntityManager();
            em.getTransaction().begin();
            em.merge(selectedStudent);
            em.getTransaction().commit();
        }
        selectedStudent = new Students();
        loadStudents();
    }

    public void loadStudents() {
        listStudents = null;

        EntityManager em = selectedStudent.getEntityManager();
        TypedQuery<Students> consultaStudents = em.createNamedQuery("Students.findAll", Students.class);
        listStudents = consultaStudents.getResultList();
    }

    public Students getSelectedStudent() {
        return selectedStudent;
    }

    public void setSelectedStudent(Students selectedStudent) {
        this.selectedStudent = selectedStudent;
    }

    public List<Students> getListStudents() {
        return listStudents;
    }

    public void setListStudents(List<Students> listStudents) {
        this.listStudents = listStudents;
    }

    public List<Students> getListStudentsByProgram() {
        return listStudentsByProgram;
    }

    public void setListStudentsByProgram(List<Students> listStudentsByProgram) {
        this.listStudentsByProgram = listStudentsByProgram;
    }

}

