
DROP TABLE `inscripcion_online`.`Students`;

CREATE SCHEMA `inscripcion_online` ;

CREATE TABLE `inscripcion_online`.`Students` ( 
  `idStudent` INT NOT NULL AUTO_INCREMENT,
  `names` VARCHAR(45) NULL,
  `lastNames` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`idStudent`));


CREATE TABLE `inscripcion_online`.`Inscriptions` (
  `idInscriptions` INT NOT NULL AUTO_INCREMENT,
  `idStudent` INT NULL,
  `cDate` DATETIME NULL,
  `program` VARCHAR(45) NULL,
  `PINCode` VARCHAR(45) NULL,
  `ICFESScore` VARCHAR(45) NULL,
  PRIMARY KEY (`idInscriptions`));


ALTER TABLE `inscripcion_online`.`Inscriptions` 
ADD INDEX `fk_Inscriptions_1_idx` (`idStudent` ASC);
ALTER TABLE `inscripcion_online`.`Inscriptions` 
ADD CONSTRAINT `fk_Inscriptions_1`
  FOREIGN KEY (`idStudent`)
  REFERENCES `inscripcion_online`.`Students` (`idStudent`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;





