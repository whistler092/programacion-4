/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import beans.Producto;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author estudiante302
 */
@Named(value = "ventaProductos")
@SessionScoped
public class VentaProductos implements Serializable {

    /**
     * Creates a new instance of VentaProductos
     */
    
    private Producto producto;
    
    public VentaProductos() {
        producto = new Producto();
        
        //producto.setUnitValue(1500);
    }

    public void calcular(){
        double subtotal = producto.getUnitValue() * producto.getQuantity();
        double iva = subtotal * 0.19;
        double discount = 0;
        
        if(producto.getQuantity() > 10 ){
            discount = subtotal * 0.20;           
        }
        
        producto.setIVA(iva);
        producto.setDiscount(discount);
        producto.setTotal(subtotal + iva - discount);
        
    }
    
    /**
     * @return the producto
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
}
