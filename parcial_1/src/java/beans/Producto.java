/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author estudiante302
 */
public class Producto {

    private double UnitValue;
    private double Quantity;
    private double IVA;
    private double Discount;
    private double Total;

    /**
     * @return the UnitValue
     */
    public double getUnitValue() {
        return UnitValue;
    }

    /**
     * @param UnitValue the UnitValue to set
     */
    public void setUnitValue(double UnitValue) {
        this.UnitValue = UnitValue;
    }

    /**
     * @return the Quantity
     */
    public double getQuantity() {
        return Quantity;
    }

    /**
     * @param Quantity the Quantity to set
     */
    public void setQuantity(double Quantity) {
        this.Quantity = Quantity;
    }

    /**
     * @return the IVA
     */
    public double getIVA() {
        return IVA;
    }

    /**
     * @param IVA the IVA to set
     */
    public void setIVA(double IVA) {
        this.IVA = IVA;
    }

    /**
     * @return the Discount
     */
    public double getDiscount() {
        return Discount;
    }

    /**
     * @param Discount the Discount to set
     */
    public void setDiscount(double Discount) {
        this.Discount = Discount;
    }

    /**
     * @return the Total
     */
    public double getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(double Total) {
        this.Total = Total;
    }

}
