CREATE TABLE `computadores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Marca` varchar(255) DEFAULT NULL,
    `Modelo` varchar(255) DEFAULT NULL,
    `CapacidadRam` int(32) DEFAULT NULL,
    `Procesador` varchar(255) DEFAULT NULL,
    `CapacidadHHD` int(32) DEFAULT NULL,
    `Precio` int(32) DEFAULT NULL,
    `Fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
