package logica;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-23T20:52:48")
@StaticMetamodel(Computadores.class)
public class Computadores_ { 

    public static volatile SingularAttribute<Computadores, String> marca;
    public static volatile SingularAttribute<Computadores, Date> fecha;
    public static volatile SingularAttribute<Computadores, Integer> precio;
    public static volatile SingularAttribute<Computadores, Integer> capacidadHHD;
    public static volatile SingularAttribute<Computadores, Integer> id;
    public static volatile SingularAttribute<Computadores, Integer> capacidadRam;
    public static volatile SingularAttribute<Computadores, String> procesador;
    public static volatile SingularAttribute<Computadores, String> modelo;

}