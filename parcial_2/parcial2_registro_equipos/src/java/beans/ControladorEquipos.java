/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import logica.Computadores;

/**
 *
 * @author estudiante302
 */
@Named(value = "controladorEquipos")
@SessionScoped
public class ControladorEquipos implements Serializable {

    /**
     * Creates a new instance of ControladorEquipos
     */
    Computadores selectedComputer;
    List<Computadores> listaEquipos;
    ArrayList<Computadores> listaEquiposPorMarca;
    List<String> listaMarcas;

    String filtrarMarcaSeleccionada = "";

    Integer filterFromRAM = 0;
    Integer filterToRAM = 0;

    Integer filterFromHHD = 0;
    Integer filterToHHD = 0;

    public ControladorEquipos() {
        this.selectedComputer = new Computadores();
        cargarEquipos();
    }

    public void cargarEquipos() {
        listaEquipos = null;
        listaEquiposPorMarca = new ArrayList<>();
        Computadores ningunComputador = new Computadores();
        ningunComputador.setMarca("");
        listaEquiposPorMarca.add(ningunComputador);

        EntityManager em = selectedComputer.getEntityManager();
        TypedQuery<Computadores> consultaComputadores = em.createNamedQuery("Computadores.findAll", Computadores.class);
        listaEquipos = consultaComputadores.getResultList();

        for (Computadores equipo : listaEquipos) {
            boolean existe = false;

            
            /*for (Computadores porMarca : listaEquiposPorMarca) {
                if (porMarca.getMarca().equals(equipo.getMarca())) {
                    existe = true;
                }
            }*/
            for (int i = 0; i < listaEquiposPorMarca.size(); i++) {
                if(listaEquiposPorMarca.get(i).getMarca() == null ? equipo.getMarca() == null : listaEquiposPorMarca.get(i).getMarca().toUpperCase().trim().equals(equipo.getMarca().toUpperCase().trim())){
                    existe = true;
                }
            }

            if (!existe) {
                listaEquiposPorMarca.add(equipo);
            }
        }

    }

    public void guardarComputador() {
        try {
            EntityManager em = selectedComputer.getEntityManager();
            em.getTransaction().begin();
            selectedComputer.setFecha(new Date());
            em.persist(selectedComputer);
            em.getTransaction().commit();

            selectedComputer = new Computadores();
            cargarEquipos();

            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "index.xhtml");

        } catch (Exception e) {
            System.out.println("Error:> " + e.getMessage());
        }
    }

    /*
    Busquedas
     */
    public void filterByRAM() {
        listaEquipos = null;

        EntityManager em = selectedComputer.getEntityManager();
        TypedQuery<Computadores> consultaComputadores = em.createNamedQuery("Computadores.findByCapacidadRam", Computadores.class);
        consultaComputadores.setParameter("fromRAM", filterFromRAM);
        consultaComputadores.setParameter("toRAM", filterToRAM);
        listaEquipos = consultaComputadores.getResultList();

        setFilterFromRAM(0);
        setFilterToRAM(0);
    }

    public void filterByHHD() {
        listaEquipos = null;

        EntityManager em = selectedComputer.getEntityManager();
        TypedQuery<Computadores> consultaComputadores = em.createNamedQuery("Computadores.findByCapacidadHHD", Computadores.class);
        consultaComputadores.setParameter("fromHHD", filterFromHHD);
        consultaComputadores.setParameter("toHHD", filterToHHD);
        listaEquipos = consultaComputadores.getResultList();

        setFilterFromRAM(0);
        setFilterToRAM(0);
    }
    
    public void filterByMarca() {
        listaEquipos = null;

        EntityManager em = selectedComputer.getEntityManager();
        TypedQuery<Computadores> consultaComputadores = em.createNamedQuery("Computadores.findByMarca", Computadores.class);
        consultaComputadores.setParameter("marca", filtrarMarcaSeleccionada);
        listaEquipos = consultaComputadores.getResultList();

         
    }

    public Computadores getSelectedComputer() {
        return selectedComputer;
    }

    public void setSelectedComputer(Computadores selectedComputer) {
        this.selectedComputer = selectedComputer;
    }

    public List<Computadores> getListaEquipos() {
        return listaEquipos;
    }

    public void setListaEquipos(List<Computadores> listaEquipos) {
        this.listaEquipos = listaEquipos;
    }

    public Integer getFilterFromRAM() {
        return filterFromRAM;
    }

    public void setFilterFromRAM(Integer filterFromRAM) {
        this.filterFromRAM = filterFromRAM;
    }

    public Integer getFilterToRAM() {
        return filterToRAM;
    }

    public void setFilterToRAM(Integer filterToRAM) {
        this.filterToRAM = filterToRAM;
    }

    public Integer getFilterFromHHD() {
        return filterFromHHD;
    }

    public void setFilterFromHHD(Integer filterFromHHD) {
        this.filterFromHHD = filterFromHHD;
    }

    public Integer getFilterToHHD() {
        return filterToHHD;
    }

    public void setFilterToHHD(Integer filterToHHD) {
        this.filterToHHD = filterToHHD;
    }

    public List<String> getListaMarcas() {
        return listaMarcas;
    }

    public void setListaMarcas(List<String> listaMarcas) {
        this.listaMarcas = listaMarcas;
    }

    public String getFiltrarMarcaSeleccionada() {
        return filtrarMarcaSeleccionada;
    }

    public void setFiltrarMarcaSeleccionada(String filtrarMarcaSeleccionada) {
        this.filtrarMarcaSeleccionada = filtrarMarcaSeleccionada;
    }

    public ArrayList<Computadores> getListaEquiposPorMarca() {
        return listaEquiposPorMarca;
    }

    public void setListaEquiposPorMarca(ArrayList<Computadores> listaEquiposPorMarca) {
        this.listaEquiposPorMarca = listaEquiposPorMarca;
    }

}
