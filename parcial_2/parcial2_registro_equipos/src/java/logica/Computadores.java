/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author estudiante302
 */
@Entity
@Table(name = "computadores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Computadores.findAll", query = "SELECT c FROM Computadores c")
    , @NamedQuery(name = "Computadores.findById", query = "SELECT c FROM Computadores c WHERE c.id = :id")
    , @NamedQuery(name = "Computadores.findByMarca", query = "SELECT c FROM Computadores c WHERE c.marca = :marca")
    , @NamedQuery(name = "Computadores.findByModelo", query = "SELECT c FROM Computadores c WHERE c.modelo = :modelo")
    , @NamedQuery(name = "Computadores.findByCapacidadRam", query = "SELECT c FROM Computadores c WHERE c.capacidadRam >= :fromRAM and c.capacidadRam <= :toRAM")
    , @NamedQuery(name = "Computadores.findByProcesador", query = "SELECT c FROM Computadores c WHERE c.procesador = :procesador")
    , @NamedQuery(name = "Computadores.findByCapacidadHHD", query = "SELECT c FROM Computadores c WHERE c.capacidadHHD >= :fromHHD and c.capacidadHHD <= :toHHD")
    , @NamedQuery(name = "Computadores.findByPrecio", query = "SELECT c FROM Computadores c WHERE c.precio = :precio")
    , @NamedQuery(name = "Computadores.findByFecha", query = "SELECT c FROM Computadores c WHERE c.fecha = :fecha")})
public class Computadores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "Marca")
    private String marca;
    @Size(max = 255)
    @Column(name = "Modelo")
    private String modelo;
    @Column(name = "CapacidadRam")
    private Integer capacidadRam;
    @Size(max = 255)
    @Column(name = "Procesador")
    private String procesador;
    @Column(name = "CapacidadHHD")
    private Integer capacidadHHD;
    @Column(name = "Precio")
    private Integer precio;
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    public Computadores() {
    }

    public Computadores(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getCapacidadRam() {
        return capacidadRam;
    }

    public void setCapacidadRam(Integer capacidadRam) {
        this.capacidadRam = capacidadRam;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public Integer getCapacidadHHD() {
        return capacidadHHD;
    }

    public void setCapacidadHHD(Integer capacidadHHD) {
        this.capacidadHHD = capacidadHHD;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Computadores)) {
            return false;
        }
        Computadores other = (Computadores) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "logica.Computadores[ id=" + id + " ]";
    }
    
    
    public EntityManager getEntityManager(){
        //punto-de-ventaPU
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("parcial2_registro_equiposPU");
        EntityManager em = emf.createEntityManager();
        return em;
    }
}
